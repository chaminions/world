<?php require_once 'includes/header.php'; ?>

<main role="main" class="flex-shrink-0">
    <?php
    require_once 'includes/manager-db.php';

    if (isset($_GET['continent'])) {
        $selectedContinent = $_GET['continent'];
        $pays = getCountriesByContinent($selectedContinent);
        $nb = count($pays);
        ?>
        <div class="row" style="margin: 0;">
            <h1>Les pays en <?php echo $selectedContinent; ?> </h1>
            <table class="table table-striped">
                <thead class="thead-dark" style="text-align: center;">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Code</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Continent</th>
                    <th scope="col">Région</th>
                    <th scope="col">Surface</th>
                    <th scope="col">Année d'indépendance</th>
                    <th scope="col">Population</th>
                    <th scope="col">Espérance de vie</th>
                    <th scope="col">PNB</th>
                </tr>
                </thead>
                <tbody style="text-align: center;">
                <?php for ($i = 0; $i < $nb; $i++):
                    $filename = "images/drapeau/" . strtolower($pays[$i]->Code2) . ".png"; ?>
                    <tr>
                        <td data-label="ID"><?php echo $pays[$i]->id; ?></td>
                        <td data-label="Code">
                            <?php if (file_exists($filename)): ?>
                                <img src="<?php echo $filename; ?>" alt="<?php echo $pays[$i]->Code2; ?>"/>
                            <?php else: print($pays[$i]->Code); endif; ?>
                        </td>
                        <td data-label="Name">
                            <a href="https://wikipedia.org/wiki/<?php echo $pays[$i]->Name; ?>" target="_blank">
                                <?php echo $pays[$i]->Name; ?>
                            </a>
                        </td>
                        <td data-label="Continent"><?php echo $pays[$i]->Continent; ?></td>
                        <td data-label="Region"><?php echo $pays[$i]->Region; ?></td>
                        <td data-label="Surface Area"><?php echo $pays[$i]->SurfaceArea; ?></td>
                        <td data-label="Independant Year"><?php echo $pays[$i]->IndepYear; ?></td>
                        <td data-label="Population"><?php echo $pays[$i]->Population; ?></td>
                        <td data-label="Life Expectancy"><?php echo $pays[$i]->LifeExpectancy; ?></td>
                        <td data-label="GNP"><?php echo $pays[$i]->GNP; ?></td>
                    </tr>
                <?php endfor; ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
        <div class="container">
            <div class="row h-100">
                <div class="col-md-2"></div>
                <div class="col-md-7 shadow box container-fluid" style="padding-top: 10px;">
                    <h2>Sélectionnez un continent</h2>
                    <hr/>
                    <div class="text-center">
                        <img src="images/map.png" usemap="#Map" width="563" height="288" class="img-fluid"
                             alt="Carte du monde">
                    </div>
                    <map name="Map" id="Map">
                        <area target="_self" alt="Amérique du sud" title="Amérique du sud" data-nbmembre="40 pays"
                              href="?continent=South America"
                              coords="152,136,107,147,109,200,143,286,167,286,161,255,206,186" shape="poly">
                        <area target="_self" alt="Amérique du Nord" title="Amérique du Nord" data-nbmembre="40 pays"
                              href="?continent=North America"
                              coords="242,0,135,6,90,17,35,25,11,43,6,59,44,52,48,66,45,84,50,102,60,125,73,139,88,144,107,149,126,136,142,129,143,118,127,103,148,81,170,74,194,53,232,34,250,7"
                              shape="poly">
                        <area target="_self" alt="Europe" title="Europe" href="?continent=Europe"
                              data-nbmembre="40 pays"
                              coords="245,94,264,89,278,86,282,93,301,93,318,95,324,82,338,82,341,74,339,62,346,59,347,48,355,42,356,35,358,27,353,22,343,19,325,23,311,23,300,23,285,23,276,27,267,36,266,49,256,47,248,47,244,55,244,64,249,70,244,74,241,81,238,91"
                              shape="poly">
                        <area target="_self" alt="Afrique" title="Afrique" href="?continent=Africa"
                              data-nbmembre="40 pays"
                              coords="246,94,256,94,268,90,279,88,282,98,294,98,304,98,313,99,320,101,320,109,323,115,327,123,331,132,338,138,344,142,350,141,354,145,353,152,350,158,346,164,342,168,335,172,335,181,341,185,351,191,352,199,350,208,346,214,342,221,333,222,326,220,320,225,316,231,312,236,302,240,294,240,287,233,281,224,281,211,278,202,280,196,282,186,278,175,276,164,267,162,257,159,245,159,236,156,228,147,227,133,223,121,233,108,240,104"
                              shape="poly">
                        <area target="_self" alt="Asia" title="Asie" href="?continent=Asia" data-nbmembre="40 pays"
                              coords="356,20,360,7,387,12,416,20,446,18,478,26,509,28,516,36,516,46,512,54,510,62,502,73,504,87,503,97,491,108,483,117,488,133,494,147,504,163,516,169,520,174,516,179,512,184,504,188,493,189,478,193,462,190,447,186,434,172,429,159,430,148,426,134,418,127,413,135,410,144,412,152,405,160,396,152,389,137,384,127,376,118,371,121,362,131,350,137,341,140,335,132,330,123,322,116,319,104,319,93,322,81,332,83,338,81,340,70,340,62,348,57,348,49,355,44,358,30"
                              shape="poly">
                        <area target="_self" alt="Oceania" title="Océanie" href="?continent=Oceania"
                              data-nbmembre="40 pays"
                              coords="521,173,529,173,541,171,539,182,535,190,529,195,534,209,560,229,559,244,551,252,544,256,529,249,522,246,515,255,506,259,495,257,495,246,490,240,480,240,464,242,453,235,455,221,460,213,471,207,480,196,490,194,506,189"
                              shape="poly">
                        <area target="_self" alt="Antarctica" title="Antarctique" href="?continent=Antarctica"
                              data-nbmembre="40 pays"
                              coords="187,287,217,270,226,271,246,266,287,262,326,262,359,261,384,259,417,258,441,267,438,275,427,278,420,282,426,287"
                              shape="poly">
                    </map>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        <script src="../assets/library/mapresizer.js" type="text/javascript"></script>
        <script type="text/javascript">imageMapResize();</script>
    <?php } ?>
</main>

<?php require_once 'includes/footer.php'; ?>
