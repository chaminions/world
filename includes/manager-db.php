<?php
require_once 'connect-db.php';
// Obtenir la liste de tous les pays référencés d'un continent donné

function getCountriesByContinent($continent)
{
    // pour utiliser la variable globale dans la fonction
    global $pdo;
    $query = 'SELECT * FROM country WHERE continent = :continent;';
    $prep = $pdo->prepare($query);
    $prep->bindValue(':continent', $continent, PDO::PARAM_STR);
    $prep->execute();
    // var_dump($prep);
    // var_dump($continent);
    return $prep->fetchAll();
}

// Obtenir la liste des pays
function getAllCountries()
{
    global $pdo;
    $query = 'SELECT * FROM country;';
    return $pdo->query($query)->fetchAll();
}

// Obtenir la liste des continents
function getAllContinents()
{
    global $pdo;
    $result = $pdo->query('SELECT continent FROM country GROUP BY continent;')->fetchAll();
    for ($i = 0; $i < count($result); $i++):
        $continents[] = $result[$i]->continent;
    endfor;
    sort($continents);
    return $continents;
}

// Ajout d'un utilisateur
function addUser($userData)
{
    global $pdo;
    if ($userData['role'] == "eleve" OR $userData['role'] == "enseignant"):

        $securedPassword = password_hash($userData['password'], PASSWORD_BCRYPT); // Hachage du MDP

        $result = $pdo->prepare("INSERT INTO users VALUES (:id, :nom, :prenom, :email, :password, :role)");
        $result->execute(array(
            ':id' => '',
            ':nom' => $userData['nom'],
            ':prenom' => $userData['prenom'],
            ':email' => $userData['email'],
            ':password' => $securedPassword,
            ':role' => $userData['role']
        ));
    endif;
    return true;
}

// Vérification des données envoyés lors de l'inscription
function verifData($email, $pass, $cpass)
{
    global $pdo;

    if (strlen($pass) > 5) { // Verif si le MDP est assez long
        if ($pass == $cpass) { // Verif si les deux MDP sont identiques
            $result = $pdo->prepare('SELECT COUNT(*) as total FROM users WHERE email = :email;');
            $result->bindValue(':email', $email, PDO::PARAM_STR);
            $result->execute();
            $occurence = $result->fetch();
            if ($occurence->total == 0) { // Verif si il existe un user avec la même email

                return 0; // ErrCode 0 : Tout s'est bien déroulé

            } else return 1; // ErrCode 1 : Email deja inscrit
        } else return 2; // ErrCode 2 : MDP non identique
    } else return 3; // ErrCode 3 : MDP trop court
}

// Vérification de chaque variables passés en POST (si elles ne sont pas vides)
function noEmptyPOST()
{
    foreach ($_POST as $key => $value) if (empty($value)) return false;
    return true;
}

// Connexion d'un utilisateur...
function login($email, $pass)
{
    global $pdo;
    if (!empty($email) && !empty($pass)) {

        $result = $pdo->prepare("SELECT * FROM users WHERE email = :email;");
        $result->execute(array(
            ':email' => $email
        ));

        $loginData = $result->fetch();
        if ($loginData != null) {
            $dbPass = $loginData->password;
            if (password_verify($pass, $dbPass)) {
                $_SESSION['email'] = $email;
                $_SESSION['nom'] = $loginData->nom;
                $_SESSION['prenom'] = $loginData->prenom;
                $_SESSION['role'] = $loginData->role;
                return true;
            }
        }else return 1; //ErrCode 1 : Mot de pass erroné
    } else return 2; // ErrCode 1 : Champs vides
}

// Oubli d'un mot de passe
function forgot($email)
{
    if (!empty($email)) {
        global $pdo;
        $result = $pdo->prepare('SELECT COUNT(*) as nb FROM users WHERE email = :email;');
        $result->execute(array(
            ':email' => $email
        ));
        $result = $result->fetch();

        if ($result->nb > 0) {
            $MDPtemporaire = rand(0, 1500) . md5(date("m-d-y") . rand(0, 42));
            $MDPtemporaire = substr($MDPtemporaire, 0, 15);
            $hashMDP = password_hash($MDPtemporaire, PASSWORD_BCRYPT);

            $MDPupdate = $pdo->prepare('UPDATE users SET password = :pass WHERE email = :email;');
            $MDPupdate->execute(array(
                ':email' => $email,
                ':pass' => $hashMDP
            ));

            $message = "Bonjour, voici votre nouveau mot de passe (modifiable) : " . $MDPtemporaire . "\r\n \r\n  Si vous n'etes pas a l'origine de cette demande, signalez-le a admin@pheox.ovh";
            $mail = mail($email, "GeoWorld - Votre nouveau mot de passe", $message);

            if ($mail) {
                return true;
            }
        }
    }
    return false;
}

// Vérification avec reCaptcha v3
function reCaptcha($response)
{
        // Build POST request:
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret = '6LcfmI4UAAAAAEMDJQnMwxEFHBf5ONglgEmmkRvT';

        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $response);
        $recaptcha = json_decode($recaptcha);

        // Take action based on the score returned:
        if ($recaptcha->score >= 0.5) {
            return true;
        } else return false;
}