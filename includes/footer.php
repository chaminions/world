<hr/>
<footer class="footer mt-auto py-3">
    <div class="container">
        <span class="text-muted">GeoWorld &copy; 2019 Chaminions</span>
    </div>
</footer>
<script src="../assets/library/custom.js" type="text/javascript"
        integrity="sha256-N70q9rneyYCliBZSKMWCF7n2idW/+SAfer4hRAByZIQ=" crossorigin="anonymous"></script>
<?php if (isset($loginResult) && ($loginResult != "true")): ?>
    <script src="../assets/library/loginError.js" type="text/javascript"
            integrity="sha256-2OR6gI8QAiUk57jJnYOlmpNCP/XSnETn2pMqbQoKZhg=" crossorigin="anonymous"></script>
<?php endif; ?>
</body>
</html>
