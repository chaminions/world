<?php
header("Content-Security-Policy: default-src 'none'; style-src 'self' 'unsafe-inline'; frame-src 'self' https://www.google.com/; frame-ancestors 'self'; font-src 'self'; img-src 'self'; object-src 'none'; script-src 'self' 'unsafe-inline' https://code.jquery.com/ https://stackpath.bootstrapcdn.com https://www.google.com https://cors.io https://www.gstatic.com; base-uri 'self'; form-action 'self';");
header("X-XSS-Protection: 1; mode=block");
header("X-Frame-Options: SAMEORIGIN");
header("X-Content-Type-Options: nosniff");
header("Strict-Transport-Security: max-age=63072000");
header('Access-Control-Allow-Origin: *');

ini_set('session.cookie_httponly', 1);
ini_set('session.cookie_secure', 1);
ini_set('session.cookie_domain', "world.ppe.ovh");

session_start();

error_reporting(-1);
ini_set('display_errors', 'On');

require_once 'includes/manager-db.php';

if (isset($_GET['logout'])) {
    session_destroy();
    header("Location: index.php");
}

if (isset($_POST['login']) && reCaptcha($_POST['reCaptchaLogin'])) $loginResult = login($_POST['email'], $_POST['password']);
?>
<!DOCTYPE html>
<html lang="fr" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta http-equiv="Referrer-Policy" content="no-referrer, strict-origin-when-cross-origin">
    <title>Project : GeoWorld</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/bootstrap-4.2.1-dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/custom.css" rel="stylesheet">
    <link rel="icon" href="../images/favicon.png" sizes="32x32" type="image/png">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cors.io/?https://www.google.com/recaptcha/api.js?render=6LcfmI4UAAAAANK5t6RL2gCSzcTT5TcFIFodCuiJ" integrity="sha256-h2c6P4TwDUI3ZsFGHMI3p8j8CfQT9YHb2a55UUH0Gpw=" crossorigin="anonymous"></script>
</head>
<body class="d-flex flex-column h-100">
<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="../index.php">GeoWorld</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Accueil <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Continents</a>
                    <div class="dropdown-menu">
                        <?php
                        $getContinents = getAllContinents();
                        $nb = count($getContinents);
                        for ($i = 0; $i < $nb; $i++): ?>
                            <a class="dropdown-item"
                               href="../index.php?continent=<?php echo $getContinents[$i]; ?>"><?php echo $getContinents[$i]; ?></a>
                        <?php endfor; ?>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <?php if (isset($_SESSION['email'])) { ?>
                    <li class="nav-item" style="margin-right: 10px;color:white;">
                        <a>Bienvenue <?php echo $_SESSION['nom'] . " " . $_SESSION['prenom']; ?></a>
                        <?php if ($_SESSION['role'] == "admin"): ?>
                            <a href="admin.php" style="text-decoration: none;">
                                <button type="button" class="btn btn-outline-danger">Admin</button>
                            </a>
                        <?php endif; ?>
                        <a href="compte.php" style="text-decoration: none;">
                            <button type="button" class="btn btn-outline-primary">Compte</button>
                        </a>
                        <a href="?logout" style="text-decoration: none;">
                            <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-off"></span>
                                Logout
                            </button>
                        </a>
                    </li>
                <?php } else { ?>
                <li class="nav-item" style="margin-right: 10px;">
                    <button type="button" class="btn btn-outline-primary" data-toggle="modal"
                            data-target=".bd-example-modal-sm">Login
                    </button>
                </li>
                <li class="nav-item">
                    <a href="register.php" style="text-decoration: none;">
                        <button type="button" class="btn btn-primary">Register</button>
                    </a>
                </li>
            </ul>
            <?php } ?>
            <!-- BARRE DE RECHERCHE
            <form class="form-inline my-2 my-lg-0">
              <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
              <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form> -->
        </div>
    </nav>
    <?php
    include 'includes/login.php';
    include 'includes/forgot.php';
    ?>
</header>
<div style="position: fixed; bottom: 0; right: 0; padding: 5px;">
    <a href="../todo-projet.php">ProjetPPE-SLAM</a>
</div>