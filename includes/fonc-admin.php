<?php
function getUserData($id, $conn) {
    $sql = ("SELECT * FROM users WHERE idusers=$id");
    $result = mysqli_query($conn, $sql) or die ("Echec de la requete : " . $sql);
    $row = mysqli_fetch_assoc($result);

    $UserData = [
        $row['Id'],
        $row['nom'],
        $row['prenom'],
        $row['email'],
        $row['password'],
        $row['role']
    ];
    return $UserData;
}