<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="ModalLogin"
     aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST">
                <div class="modal-body">
                    <?php if (isset($loginResult) && ($loginResult != "true")):
                        switch ($loginResult) {
                            case 1:
                                ?>
                                <div class="alert alert-danger" role="alert"><strong>Erreur! </strong>
                                    Mauvais identifiants !
                                </div>
                                <?php break;
                            case 2: ?>
                                <div class="alert alert-danger" role="alert"><strong>Erreur! </strong>
                                    Veuillez remplir tout les champs !
                                </div>
                                <?php break;
                        } endif; ?>
                    <div class="row">
                        <div class="col">
                            E-Mail
                            <input type="email" class="form-control" placeholder="Adresse e-mail" name="email" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            Mot de Passe
                            <input type="password" class="form-control" placeholder="••••••••••" autocomplete="off"
                                   name="password" required>
                            <a href="?forgot">Mot de passe oublié ?</a>
                        </div>
                    </div>
                    <input type="hidden" id="reCaptchaLogin" name="reCaptchaLogin" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary float-right" name="login">Se connecter</button>
                </div>
            </form>
        </div>
    </div>
</div>