<?php if (isset($_GET['forgot'])): ?>
    <div class="modal fade bd-example-modal-sm" id="ModalForgot">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Mot de passe oublié ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST">
                    <div class="modal-body">
                        <?php
                        if (isset($_POST['forgot'])) {
                            if (isset($_POST['reCaptchaForgot']) && reCaptcha($_POST['reCaptchaForgot'])) {
                                if (forgot($_POST['forgotEmail'])) { ?>
                                    <div class="alert alert-success" role="alert">
                                        <h4 class="alert-heading">OK !</h4>
                                        Votre mot de passe a été réinitialisé, vérifiez votre boite mail !
                                    </div>
                                <?php } elseif (forgot($_POST['forgotEmail']) == false) { ?>
                                    <div class="alert alert-warning" role="alert">
                                        <h4 class="alert-heading">Erreur !</h4>
                                        Cette adresse e-mail n'est pas inscrite sur GeoWorld !
                                    </div>
                                <?php }
                            }
                        } ?>
                        <div class="row">
                            <div class="col">
                                E-Mail
                                <input type="email" class="form-control" placeholder="Adresse e-mail" name="forgotEmail"
                                       required>
                            </div>
                        </div>
                        <input type="hidden" id="reCaptchaForgot" name="reCaptchaForgot" value=""/>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary float-right" name="forgot">Envoyer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>