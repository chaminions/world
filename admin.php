<?php require_once 'includes/header.php';
require_once 'includes/fonc-admin.php';
if (isset($_SESSION['role']) && ($_SESSION['role'] == "admin")) {
    ?>
    <main role="main" class="flex-shrink-0">
        <div class="container">
            <div class="row h-100">
                <div class="col-sm-1"></div>
                <div class="col-lg-12 shadow box container-fluid" style="padding: 0;">
                    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01"
                                aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                            <a class="navbar-brand" href="#">Administration</a>
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Utilisateurs</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Requêtes SQL</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </main>
<?php } else { ?>
    <div class="alert alert-danger" role="alert"><strong>Avertissement</strong>
        Vous vous êtes perdu ? Ou vous avez envie d'être banni ?
    </div>
    <?php
}
require_once 'includes/footer.php'; ?>
