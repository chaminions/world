<?php require_once 'includes/header.php';
if (isset($_POST['inscription'])) {
    $err = verifData($_POST['email'], $_POST['password'], $_POST['cpassword']);
    if ($err == 0) {
        if (noEmptyPOST()) addUser($_POST); else $err = 4;
    }
}
?>
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col"
                 style="border: 1px solid lightblue; padding: 15px;">
                <h3>Inscription</h3>
                <?php if (isset($err)): ?>
                    <?php
                    switch ($err) { 
                        case 0: ?>
                            <div class="alert alert-success" role="alert">
                                <h4 class="alert-heading">Bien joué!</h4>
                                Votre inscription s'est déroulée avec succès, nous vous invitons dès à présent à vous
                                connecter
                            </div>
                            <?php break;
                        case 1: ?>
                            <div class="alert alert-danger" role="alert"><strong>Erreur! </strong>
                                Vous avez déjà un compte avec cette adresse email !
                            </div>
                            <?php break;
                        case 2: ?>
                            <div class="alert alert-danger" role="alert"><strong>Erreur! </strong>
                                Les mots de passe ne correspondent pas !
                            </div>
                            <?php break;
                        case 3: ?>
                            <div class="alert alert-danger" role="alert"><strong>Erreur! </strong>
                                Votre mot de passe est trop court !
                            </div>
                            <?php break;
                        case 4: ?>
                            <div class="alert alert-danger" role="alert"><strong>Erreur! </strong>
                                Veuillez remplir tous les champs de texte !
                            </div>
                            <?php break;
                    } ?>
                <?php endif; ?>
                <form method="POST" action="register.php">
                    <div class="form-group">
                        <label class="form-radio-label" for="role">Qui êtes-vous ?</label>
                        <input type="radio" name="role" value="eleve" checked="checked" class="form-radio-input"
                               id="role" required>
                        Un élève
                        <input type="radio" name="role" value="enseignant" class="form-radio-input" required> Un
                        enseignant
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="nom">Nom</label>
                                <input name="nom" type="text" class="form-control" id="nom" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="prenom">Prénom</label>
                                <input name="prenom" type="text" class="form-control" id="prenom" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">Adresse email</label>
                        <input name="email" type="email" class="form-control" id="email"
                               placeholder="Entrez une adresse e-mail" required>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="password">Mot de passe</label>
                                <input name="password" type="password" class="form-control" id="password"
                                       placeholder="Mot de passe" required>
                                <small id="passwordHelp" class="form-text text-muted">Le mot de passe doit comporter au
                                    minimum 6 caractères.
                                </small>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="cpassword">Confirmez le Mot de passe</label>
                                <input name="cpassword" type="password" class="form-control" id="cpassword"
                                       placeholder="Confirmez Mot de passe" required>
                            </div>
                        </div>

                    </div>
                    <br/>
                    <button type="submit" name='inscription' value="ok" class="btn btn-primary float-right">
                        Inscription
                    </button>
                </form>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
<?php require_once 'includes/footer.php'; ?>